import asyncpg
from aiohttp import web
import random


def generate_user_json() -> dict:
    user_id = random.randint(1, 2147483645)
    allowed_chars = 'abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    login = "".join([random.choice(allowed_chars) for _ in range(random.randint(6, 14))])
    password = "".join([random.choice(allowed_chars) for _ in range(random.randint(8, 20))])

    user_json = {"user_id": user_id, "login": login, "password": password}
    return user_json


async def create_table(pool: asyncpg.pool.Pool):
    """
    Create table users if it not exist
    """
    async with pool.acquire() as connection:
        query = '''
            CREATE TABLE IF NOT EXISTS users (
            id INT, 
            login VARCHAR(14),
            password VARCHAR(20)
            )
            '''
        await connection.execute(query)


# Просмотр списка всех пользователей (id, login). В параметрах запроса ожидать offset и limit, задающие диапазон строк
async def select_users(request: web.Request) -> web.Response:
    """
    Просмотр списка всех пользователей (id, login)
    """
    try:
        offset = int(request.match_info.get("offset"))
        limit = request.match_info.get("limit")
        if limit.upper() != "ALL":
            limit = int(limit)
    except ValueError or KeyError:
        return web.Response(text="Not appropriate limit and/or offset")

    pool: asyncpg.pool.Pool = request.app["pool"]
    async with pool.acquire() as connection:
        query = f"""
            SELECT
                id, login
            FROM 
                users
            LIMIT {limit} OFFSET {offset}
            """
        result = await connection.fetch(query)
    text = "\n".join(list(map(str, result)))
    return web.Response(text=text)


# Запрос пароля пользователя по его id, который ожидается в match_info.
async def get_pass_by_id(request: web.Request) -> web.Response:
    """
    Запрос пароля пользователя по его id
    """
    try:
        user_id = int(request.match_info.get("user_id"))
    except KeyError or ValueError:
        return web.Response(text="Not appropriate user_id")

    pool: asyncpg.pool.Pool = request.app["pool"]
    async with pool.acquire() as connection:
        query = f"""
                SELECT password
                FROM users
                WHERE id={user_id};
                """
        result = await connection.fetch(query)

    text = "This user does not exist." \
        if not result else \
        "\n".join(list(map(str, result)))

    return web.Response(text=text)


# Добавление нового пользователя. Данные ожидать в формате json.
async def add_user(request: web.Request) -> web.Response:
    """
    Добавление нового пользователя.
    """
    user_json = generate_user_json()
    user_id, login, password = user_json["user_id"], user_json["login"], user_json["password"]

    pool: asyncpg.pool.Pool = request.app["pool"]
    async with pool.acquire() as connection:
        query = f"""
            INSERT INTO users 
            VALUES({user_id}, '{login}', '{password}');
            """
        await connection.execute(query)

    text = f"User {user_id} was added"
    return web.Response(text=text)


# Изменение пароля пользователя по его id. Ожидать match_info и json.
async def update_pass_by_id(request: web.Request) -> web.Response:
    """
    Изменение пароля пользователя по его id.
    """
    try:
        user_id = int(request.match_info.get("user_id"))
    except KeyError or ValueError:
        return web.Response(text="Not appropriate user_id")

    user_json = generate_user_json()
    password = user_json["password"]

    pool: asyncpg.pool.Pool = request.app["pool"]
    async with pool.acquire() as connection:
        query = f"""
                UPDATE users
                SET password = '{password}'
                WHERE id = {user_id}
                """
        await connection.execute(query)

    text = f"User {user_id} was updated"
    return web.Response(text=text)


# Удаление пользователя.
async def delete_user_by_id(request: web.Request) -> web.Response:
    """
    Удаление пользователя по его id
    """
    try:
        user_id = int(request.match_info.get("user_id"))
    except KeyError or ValueError:
        return web.Response(text="Not appropriate user_id")

    pool: asyncpg.pool.Pool = request.app["pool"]
    async with pool.acquire() as connection:
        query = f"""
                DELETE 
                FROM users
                WHERE id = {user_id};
                """
        await connection.execute(query)
    text = f"User {user_id} was delete"
    return web.Response(text=text)


if __name__ == "__main__":
    print("Hello ")
