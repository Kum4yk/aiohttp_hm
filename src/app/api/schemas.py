from marshmallow import Schema, fields


class NameReqSchema(Schema):
    """Схема запроса демо-хендлера."""
    name = fields.Str(required=True)


class VersionRespSchema(Schema):
    """Схема ответа версии."""
    version = fields.Str(required=True)


class SelectReqSchema(Schema):
    """Схема запроса выбора пользователей"""
    limit = fields.Int(required=True)
    offset = fields.Int(required=True)


class CheckIdReqSchema(Schema):
    """Схема проверки user_id"""
    user_id = fields.Int(required=True)


