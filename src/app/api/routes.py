from aiohttp import web
import aiohttp_cors
from app.api.handlers.handlers import handle_example, get_version, hello
from app.api.handlers.db_handler import select_users, get_pass_by_id, add_user, update_pass_by_id, delete_user_by_id


def setup_routes(app: web.Application):
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*"
        )
    })

    app.router.add_route("GET", "/", hello)
    app.router.add_route("GET", "/{name}", handle_example)
    app.router.add_route("GET", "/api/version", get_version)
    app.router.add_route("GET", "/check/select/{limit}/{offset}", select_users)
    app.router.add_route("GET", "/check/get_pass/{user_id}", get_pass_by_id)
    app.router.add_route("GET", "/check/add_user", add_user)
    app.router.add_route("GET", "/check/update_pass/{user_id}", update_pass_by_id)
    app.router.add_route("GET", "/check/delete_user/{user_id}", delete_user_by_id)

    for route in list(app.router.routes()):
        cors.add(route)
