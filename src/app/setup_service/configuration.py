import argparse
from trafaret import Dict, String, Int, Bool
from trafaret_config import commandline
from pathlib import Path

MINIMAL_USERSPACE_PORT = 1001
TRAFARET = Dict({
    'service': Dict({
        "name": String,
        "port": Int(gte=MINIMAL_USERSPACE_PORT),
        "version": String,
        "debug": Bool
    })
})


def get_config(args):
    ap = argparse.ArgumentParser()
    commandline.standard_argparse_options(
        ap,
        default_config=Path(__file__).parent.parent.parent / "config" / "default.yaml"
    )

    options = ap.parse_args()
    return commandline.config_from_options(options, TRAFARET)
