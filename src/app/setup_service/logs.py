import logging


def init_logger(config):
    log_level = logging.DEBUG if config["service"]["debug"] else logging.INFO
    logging.basicConfig(
        level=log_level,
        format=(
            f"{config['service']['name']} - "
            f"{config['service']['version']} - "
            '%(asctime)s - %'
            '%(levelname)s -'
            '%(message)s'
        ),
    )
