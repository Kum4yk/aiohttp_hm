from aiohttp import web
import asyncio
import asyncpg
from app.api.routes import setup_routes
from app.setup_service.configuration import get_config
from app.setup_service.logs import init_logger
from app.setup_service.middlewares import log_middleware, error_middleware
from app.api.handlers.db_handler import create_table


async def init_app(sql_params: dict, args: int = 0) -> web.Application:
    """Initialize the application server."""
    app = web.Application(
        middlewares=[log_middleware, error_middleware]
    )
    app["config"] = get_config(args)

    # create connection
    app["pool"]: asyncpg.pool.Pool = await asyncpg.create_pool(**sql_params)
    await create_table(app["pool"])

    setup_routes(app)
    init_logger(app["config"])
    return app


if __name__ == "__main__":
    params = {
        'database': 'aiohttp_pool-hm',
        'host': 'localhost',
        'user': 'postgres',
        'password': '123123'
    }

    print("\n\n\nGo here - http://localhost:8088/")

    # start(final_model, params)
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(init_app(params))
    web.run_app(
        app,
        port=app["config"]["service"]["port"],
        access_log=None
    )
